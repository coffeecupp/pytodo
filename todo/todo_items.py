from typing import List


class TodoItem(object):
    def __init__(self, title: str = "", description: str = "") -> None:
        self._title = title
        self._description = description
        self._priority: List[str] = ['Todo', 'In Progress', 'Done']

    @property
    def title(self) -> str:
        return self._title

    @title.setter
    def title(self, value: str) -> None:
        self._title = value

    @property
    def description(self) -> str:
        return self._description

    @description.setter
    def description(self, value: str) -> None:
        self._description = value

    @property
    def priority(self) -> List[str]:
        return self._priority

    @priority.setter
    def priority(self, value: List[str]) -> None:
        self._priority = value
