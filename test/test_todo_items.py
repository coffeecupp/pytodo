from todo.todo_items import TodoItem
import unittest


class TestTodoItems(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._todo = TodoItem("Test Title", "Some Description")

    @classmethod
    def tearDownClass(cls):
        cls._todo = None

    def test_instance(self):
        self.assertIsInstance(self._todo, TodoItem)

    def test_todo_title_not_empty(self):
        self.assertIsNotNone(self._todo.title)

    def test_todo_title(self):
        self.assertEqual(self._todo.title, "Test Title")
